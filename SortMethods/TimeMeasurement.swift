//
//  TimeMeasurement.swift
//  SortMethods
//
//  Created by Bohdan Hordiienko on 6/13/17.
//  Copyright © 2017 Bohdan Hordiienko. All rights reserved.
//

import Foundation

class TimeMeasurement {
    
    var startTime : DispatchTime?
    var finishTime : DispatchTime?
    
    func start() {
        
        startTime = DispatchTime.now()
    }
    
    func finish() -> Double {
        finishTime = DispatchTime.now()
        let time = (finishTime?.uptimeNanoseconds)! - (startTime?.uptimeNanoseconds)!
        let timeInterval = Double(time) / 1000000000
        
        return timeInterval
    }
}

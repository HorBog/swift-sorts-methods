//
//  SortTypes.swift
//  SortMethods
//
//  Created by Bohdan Hordiienko on 5/23/17.
//  Copyright © 2017 Bohdan Hordiienko. All rights reserved.
//

import Foundation

class SortTypes {
    
    private var numberOfElements = Int()
    private var arrayForSort: [Int] = [6, 5, 1, 7, 8, 2, 10, 3]
    private var bubleSortState = true
    
    func bubbleSort(array: [Int]) {
        var iteration = array
        
        for _ in 0 ... array.count {
            
            for indexI in 1...iteration.count - 1 {
                if iteration[indexI - 1] > iteration[indexI] {
                    let temp = iteration[indexI - 1]
                    iteration[indexI - 1] = iteration[indexI]
                    iteration[indexI] = temp
                }
            }
        }
        
        
    }
    
    
    func insertionSort(array: [Int]) {
        var b = array
        
        for firstNumberToCompare in 1..<b.count {
            let currentValue = b[firstNumberToCompare]
            var previousPosition = firstNumberToCompare - 1
            
            while previousPosition >= 0 && b[previousPosition] > currentValue {
                swap(&b[previousPosition + 1], &b[previousPosition])
                
                previousPosition -= 1
            }
        }
    }


    /*
    
    func mergeSort (array: [Int]) -> [Int] {
        
        let middleIndex = array.count / 2
        
        let leftArray = mergeSort(array: Array(array[0..<middleIndex]))
        let rightArray = mergeSort(array: Array(array[middleIndex..<array.count]))
        
        return merge(leftArray, rightArray)
    }
    
    func merge (_ left: [Int], _ right: [Int]) -> [Int] {
        var leftIndex = 0
        var rightIndex = 0
        
        var orderedArray: [Int] = []
        
        while leftIndex < left.count && rightIndex < right.count {
            let leftElement = left[leftIndex]
            let rightElement = right[rightIndex]
            
            if leftElement < rightElement {
                orderedArray.append(leftElement)
                leftIndex += 1
            } else if leftElement > rightElement {
                orderedArray.append(rightElement)
                rightIndex += 1
            } else {
                orderedArray.append(leftElement)
                leftIndex += 1
                orderedArray.append(rightElement)
                rightIndex += 1
            }
        }
        
        while leftIndex < left.count {
            orderedArray.append(left[leftIndex])
            leftIndex += 1
        }
        
        while rightIndex < right.count {
            orderedArray.append(right[rightIndex])
            rightIndex += 1
        }
        
        return orderedArray
    }
    
    func prepareForQuickSort (firstIterator: Int, secondIterator: Int, array: [Int]) {
        
        arrayForSort = array
        quickSort(firstIterator: firstIterator, secondIterator: secondIterator)
    }
    
    func quickSort(firstIterator: Int, secondIterator: Int) {
        
        var leftIndex = firstIterator
        var rightIndex = secondIterator
        
        let middlePoint = (firstIterator + secondIterator)/2
        
        var middle = arrayForSort[middlePoint]

        while leftIndex <= rightIndex {
            while arrayForSort[leftIndex] < middle {
                leftIndex += 1
            }
            while arrayForSort[rightIndex] > middle {
                rightIndex -= 1
            }
         

            if (leftIndex < rightIndex) {
                
                swap(&arrayForSort[leftIndex], &arrayForSort[rightIndex])
            }
            
            if firstIterator < rightIndex {
                
                quickSort(firstIterator: firstIterator, secondIterator: rightIndex)
            }
            if secondIterator >
                leftIndex {
                quickSort(firstIterator: leftIndex, secondIterator: secondIterator)
            }
            
            print(arrayForSort)
        }
    }
    */
    func mergeSort(values : inout [Int]){
        var temp = [Int]()
        mSort(arr: &values, lo: 0, hi: values.count - 1, buf: &temp)
    }
    
    func mSort(arr : inout [Int] , lo : Int, hi : Int , buf : inout [Int]){
        if hi <= lo { return }
        let mid = lo + (hi - lo) / 2;
        mSort(arr: &arr, lo: lo, hi: mid, buf: &buf)
        mSort(arr: &arr, lo: mid + 1, hi: hi, buf : &buf)
        buf = Array(arr[lo...hi])

        var pos1 = lo
        var pos2 = mid + 1
        var k = lo
        while pos1 <= mid && pos2 <= hi{
            if buf[pos1 - lo] < buf[pos2 - lo]{
                arr[k] = buf[pos1 - lo]
                pos1 += 1
                k += 1
            }else{
                arr[k] = buf[pos2 - lo]
                pos2 += 1
                k += 1
            }
        }
    }
    
    func quickSort(values: inout [Int]){
        qSort(arr: &values, lo: 0, hi: values.count - 1)
    }
    
    func qSort(arr: inout [Int], lo : Int, hi : Int){
        if lo >= hi {
            return
        }
        let part = partition(arr: &arr, lo: lo, hi: hi)
        qSort(arr: &arr, lo: lo, hi: part - 1)
        qSort(arr: &arr, lo: part + 1, hi: hi)
    }
    
    func partition(arr : inout [Int], lo : Int, hi : Int) -> Int{
        var i = lo
        var j = hi
        let pivot = arr[hi]
        while(true){
            while i < j && arr[i] < pivot{ i += 1 }
            while i < j && arr[j] > pivot{ j -= 1 }
            if i < j {
                let temp = arr[i]
                arr[i] = arr[j]
                arr[j] = temp
            }else{
                break
            }
        }
        return i
    }
}

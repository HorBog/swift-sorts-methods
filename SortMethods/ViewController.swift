//
//  ViewController.swift
//  SortMethods
//
//  Created by Bohdan Hordiienko on 5/23/17.
//  Copyright © 2017 Bohdan Hordiienko. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var scSortMethod: UISegmentedControl!
    private var numberOfElements = Int()
    private var arrayForSort: [Int] = [6, 5, 1, 10, 8, 2, 7, 3, 4, 9]
    private var indexI = 1
    private var indexJ = 1
    private var controlForBubble = Int()
    private var numberOfSections = 1
    private var numberOfRows = 0
    private var arrayOfArrays = [[Int]]()
    private var firstIteratedArray = [Int]()
    private var secondIteratedArray = [Int]()
    private var indexForArray = 0
    private var mergeStatus = false
    
    var measurement = Measurements()
    
    private let sortTypes = SortTypes()
    
    @IBOutlet weak var sortButton: UIButton!
    @IBOutlet weak var sortTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       // sortTypes.quickSort(firstIterator: 0, secondIterator: 7)
       // print(sortTypes.returnSortedValues())
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return numberOfSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrayForSort.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellIForSort", for: indexPath)
        cell.textLabel?.text = "\(arrayForSort[indexPath.row])"
        return cell
    }
    
    
    func bubbleSort() {
        
        if controlForBubble > 2 {
            if arrayForSort[indexJ] > arrayForSort[indexJ + 1] {
                let temp = arrayForSort[indexJ]
                arrayForSort[indexJ] = arrayForSort[indexJ + 1]
                arrayForSort[indexJ + 1] = temp
                let ipAt = IndexPath(row: indexJ, section: 0)
                let ipTo = IndexPath(row: indexJ + 1, section: 0)
                sortTableView.moveRow(at: ipAt, to: ipTo)
            }
            
            if indexJ == controlForBubble - 2 {
    
                indexJ = 0
                controlForBubble -= 1
            } else {
                
                indexJ += 1
            }
        }
    }
    
    func InsertionSort() {
        
        if indexI < arrayForSort.count {
            if  indexJ > 0 && arrayForSort[indexJ] < arrayForSort[indexJ - 1] {
                swap(&arrayForSort[indexJ - 1], &arrayForSort[indexJ])
                let ipAt = IndexPath(row: indexJ, section: 0)
                let ipTo = IndexPath(row: indexJ - 1, section: 0)
                sortTableView.moveRow(at: ipAt, to: ipTo)
            }
            indexJ -= 1
            if indexJ == 0 {
                indexI += 1
                indexJ = indexI
            }
        }
    }
    
    func mergeSort() {
        
        
        if mergeStatus == false {
            
            var paramForAppending = arrayOfArrays[indexForArray]
            var paramForChecking = paramForAppending.count / 2
            
            while paramForChecking == 0 {
                
                indexForArray += 1
                paramForAppending = arrayOfArrays[indexForArray]
                paramForChecking = paramForAppending.count / 2
            }
            
            for i in 0 ..< paramForAppending.count / 2 {
                
                firstIteratedArray.append(paramForAppending[i])
            }
            
            for i in paramForAppending.count / 2 ..< paramForAppending.count {
                
                secondIteratedArray.append(paramForAppending[i])
            }
            
            arrayOfArrays.remove(at: indexForArray)
            arrayOfArrays.insert(firstIteratedArray, at: indexForArray)
            arrayOfArrays.insert(secondIteratedArray, at: indexForArray + 1)
            firstIteratedArray = []
            secondIteratedArray = []

            
            if indexForArray == arrayOfArrays.count - 2 {
                
                indexForArray = 0
            } else {
                
                indexForArray += 2
            }
        
            if arrayOfArrays.count == arrayForSort.count {
                mergeStatus = true
                indexForArray = 0
            }
          
        } else {
            
            if indexForArray + 1 != arrayOfArrays.count {
                
                firstIteratedArray = arrayOfArrays[indexForArray]
                secondIteratedArray = arrayOfArrays[indexForArray + 1]
                
                var resultedArray = [Int]()
                
                while resultedArray.count != (firstIteratedArray.count + secondIteratedArray.count) {
                    
                    if firstIteratedArray[indexI] < secondIteratedArray[indexJ] {
                        resultedArray.append(firstIteratedArray[indexI])
                        indexI += 1
                    } else {
                        resultedArray.append(secondIteratedArray[indexJ])
                        indexJ += 1
                    }
                    
                    if resultedArray.count == (firstIteratedArray.count + secondIteratedArray.count) - 1 {
                        
                        if indexI < firstIteratedArray.count {
                            resultedArray.append(firstIteratedArray[indexI])
                            indexI += 1
                        } else if indexJ < secondIteratedArray.count {
                            resultedArray.append(secondIteratedArray[indexJ])
                            indexJ += 1
                        }
                    }
                }
                
 
                arrayOfArrays.remove(at: indexForArray)
                arrayOfArrays.remove(at: indexForArray)
                arrayOfArrays.insert(resultedArray, at: indexForArray)
                indexForArray += 1
                resultedArray = []
                indexI = 0
                indexJ = 0
                
                if indexForArray == 5 {
                    indexForArray = 0
                }
                
            } else {
                
                indexForArray = 0
            }
        }
    }
    
    @IBAction func btnIteration(_ sender: Any) {
        
        
        if scSortMethod.selectedSegmentIndex == 0 {
            InsertionSort()
            
        } else if scSortMethod.selectedSegmentIndex == 1 {
            
            bubbleSort()
        } else if scSortMethod.selectedSegmentIndex == 2 {
            
            mergeSort()
        } else {
            
        }
    }
    
    @IBAction func changeMethod(_ sender: Any) {
        if scSortMethod.selectedSegmentIndex == 0 {
            
            indexI = 1
            indexJ = 1
            sortTableView.reloadData()
        } else if scSortMethod.selectedSegmentIndex == 1 {
            
            indexJ = 0
            controlForBubble = arrayForSort.count
            sortTableView.reloadData()
        } else if scSortMethod.selectedSegmentIndex == 2 {
            
            
        } else {
            
        }
    }
    
    @IBAction func reset(_ sender: Any) {
        
        self.arrayForSort = [6, 5, 1, 10, 8, 2, 7, 3, 4, 9]
        numberOfSections = 1
        if scSortMethod.selectedSegmentIndex == 0 {
            indexI = 1
            indexJ = 1
        } else if scSortMethod.selectedSegmentIndex == 1 {
            
            indexJ = 0
            controlForBubble = arrayForSort.count
        } else if scSortMethod.selectedSegmentIndex == 2 {
            numberOfSections = 1
            indexI = 0
            indexJ = 0
            arrayOfArrays.append(arrayForSort)
            indexForArray = 0
        } else if scSortMethod.selectedSegmentIndex == 3 {
            
            
        }
        
        sortTableView.reloadData()
    }
    
    @IBAction func btnRace(_ sender: Any) {
        
        let measurementsTime = measurement.measurements(arrayType: 0)
        
        for param in measurementsTime {
            
            print("Measure \(param)")
        }
    }
}


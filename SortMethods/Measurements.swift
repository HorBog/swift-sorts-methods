//
//  Measurements.swift
//  SortMethods
//
//  Created by Bohdan Hordiienko on 6/13/17.
//  Copyright © 2017 Bohdan Hordiienko. All rights reserved.
//

import Foundation

class Measurements {
    
    var arrayGenerator = ArrayGenerator()
    var sortType = SortTypes()
    var timeMeasureBubble = TimeMeasurement()
    var timeMeasureInsert = TimeMeasurement()
    var timeMeasureMerge = TimeMeasurement()
    var timeMeasureQuick = TimeMeasurement()
    var timeMeasureDefault = TimeMeasurement()
    
    var summaryTimeFor50Times = TimeMeasurement()
    
    var sortedArray = [Int]()
    
    func measurements (arrayType: Int) -> [Double] {
        var resultedArray = [Double]()
        var timeForOne = [0.0, 0.0, 0.0, 0.0, 0.0]
        var timeForFour = [0.0, 0.0, 0.0, 0.0, 0.0]
        var timeForEight = [0.0, 0.0, 0.0, 0.0, 0.0]
        
        
        for i in 0 ... 2 {
            
            summaryTimeFor50Times.start()
            switch i {
            case 0: timeForOne = callMeasureFiftyTimes(count: 1000, arrayType: arrayType)
            case 1: timeForFour = callMeasureFiftyTimes(count: 4000, arrayType: arrayType)
            default: timeForEight = callMeasureFiftyTimes(count: 8000, arrayType: arrayType)
            }
            
            print("Iteration \(i) =  \(summaryTimeFor50Times.finish()) sec")
        }
        resultedArray = timeForOne + timeForFour + timeForEight
        
        print("RESULTED : \(resultedArray)")
        
        return resultedArray
    }
    
    func oneMeasure (array: [Int]) -> [Double]{
        
        var summaryTime: [Double] = [0.0, 0.0, 0.0, 0.0, 0.0]
        sortedArray = array
        let concurentQueue = DispatchQueue(label:"concurentQueue" , attributes: .concurrent)
        
        concurentQueue.async {
            
            self.timeMeasureBubble.start()
            self.sortType.bubbleSort(array: self.sortedArray)
            summaryTime[0] = self.timeMeasureBubble.finish()
        }
        
        concurentQueue.async {
            
            self.timeMeasureInsert.start()
            self.sortType.insertionSort(array: self.sortedArray)
            summaryTime[1] = self.timeMeasureInsert.finish()
        }
        
        concurentQueue.async {
            
            self.timeMeasureMerge.start()
            self.sortType.mergeSort(values: &self.sortedArray)
            summaryTime[2] = self.timeMeasureMerge.finish()
        }
        
        concurentQueue.async {
            
            self.timeMeasureQuick.start()
            //sortType.quickSort(values: &sortedArray)
            summaryTime[3] = self.timeMeasureQuick.finish()
        }
        
        concurentQueue.async {
            
            self.timeMeasureDefault.start()
            self.sortedArray.sort()
            summaryTime[4] = self.timeMeasureDefault.finish()
        }
        return summaryTime
    }
    
    func callMeasureFiftyTimes(count: Int , arrayType: Int) -> [Double]{
        
        var iterator = 0
        var sortedArray = [Int]()
        var time = [0.0 , 0.0, 0.0, 0.0, 0.0]
        var oneMeasureTime = [Double]()
        
        switch arrayType {
        case 0: sortedArray = arrayGenerator.randomArray(count: count)
        case 1: sortedArray = arrayGenerator.partlyOrdered(count: count)
        case 2: sortedArray = arrayGenerator.orderedAscending(count: count)
        default: sortedArray = arrayGenerator.orderedDescending(count: count)
        }
        
        while iterator < 50 {
            oneMeasureTime = oneMeasure(array: sortedArray)

            for index in 0 ... 4 {
                time[index] += oneMeasureTime[index]
            }
            iterator += 1
        }
        return time
    }
}

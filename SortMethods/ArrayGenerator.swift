//
//  ArrayGenerator.swift
//  SortMethods
//
//  Created by Bohdan Hordiienko on 6/13/17.
//  Copyright © 2017 Bohdan Hordiienko. All rights reserved.
//

import Foundation

class ArrayGenerator {
    
    func randomArray (count: Int) -> [Int] {
        var returnedArray = [Int]()
        var index = 0
        while index < count {
            returnedArray.append(Int(arc4random_uniform(UInt32(count))))
            index += 1
        }
        return returnedArray
    }
    
    func orderedAscending (count: Int) -> [Int] {
        var returnedArray = [Int]()
        var index = 0
        while index < count {
            returnedArray.append(index)
            index += 1
        }
        return returnedArray
    }
    
    func orderedDescending (count: Int) -> [Int] {
        var returnedArray = [Int]()
        var index = count
        while index != 0 {
            returnedArray.append(index)
            index -= 1
        }
        return returnedArray
    }
    
    func partlyOrdered (count: Int) -> [Int] {
        var returnedArray = [Int]()
        let index = 0
        var param = 0
        while index < count {
            returnedArray.append(param * 100 + Int(arc4random_uniform(100)))
            if param == 99 {
                param = 0
            } else {
                param += 1
            }
        }
        return returnedArray
    }
}
